module product {
    requires prices.and.currencies;

    exports product;
    exports product.identification;
}