package product;

import product.identification.ISIN;

public class Bond implements IssuedProduct {
    private final ISIN isin;
    private final String name;
    private final Issuer issuer;

    public Bond(ISIN isin, String name, Issuer issuer) {
        assert isin != null;
        assert name != null;
        assert issuer != null;

        this.isin = isin;
        this.name = name;
        this.issuer = issuer;
    }

    @Override
    public ISIN getISIN(){
        return isin;
    }

    @Override
    public String getName(){
        return name;
    }

    @Override
    public Issuer getIssuer() {
        return issuer;
    }
}
