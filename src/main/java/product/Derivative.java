package product;

import product.identification.ISIN;

public class Derivative implements IssuedProduct {
    private final ISIN isin;
    private final Issuer issuer;
    private final String name;

    public Derivative(ISIN isin, Issuer issuer, String name) {
        assert isin != null;
        assert issuer != null;
        assert name != null;

        this.issuer = issuer;
        this.name = name;
        this.isin = isin;
    }

    @Override
    public Issuer getIssuer() {
        return issuer;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public ISIN getISIN() {
        return null;
    }
}
