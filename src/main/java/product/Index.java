package product;

/**
 * This represents a Bond index (DAX, etc.)
 */
public class Index implements Product {
    private final String name;

    public Index(String name) {
        assert name != null;
        this.name = name;
    }

    @Override
    public String getName() {
        return name;
    }
}
