package product;

import product.identification.ISIN;

public interface IssuedProduct extends Product {

    class Issuer{
        private final String name;

        public Issuer(String name) {
            assert name != null;
            this.name = name;
        }

        public String getName() {
            return name;
        }
    }

    Issuer getIssuer();
    ISIN getISIN();
}
