package product;

import price.Price;

import java.time.ZonedDateTime;
import java.util.Objects;

public class ProductPrice {
    private final Product product;
    private final Price price;
    private final ZonedDateTime zonedDateTime;

    public ProductPrice(Product product, Price price, ZonedDateTime zonedDateTime) {
        assert product != null;
        assert price != null;
        assert zonedDateTime != null;

        this.product = product;
        this.price = price;
        this.zonedDateTime = zonedDateTime;
    }

    public Product getProduct() {
        return product;
    }

    public Price getPrice() {
        return price;
    }

    public ZonedDateTime getZonedDateTime() {
        return zonedDateTime;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ProductPrice that = (ProductPrice) o;
        return product.equals(that.product) &&
                price.equals(that.price);
    }

    @Override
    public int hashCode() {
        return Objects.hash(product, price);
    }

    @Override
    public String toString() {
        return "MarketPrice{" +
                "product=" + product +
                ", price=" + price +
                '}';
    }
}
