package product.identification;

public class ISIN implements IdentificationNumber{
    private final String countryCode;
    private final String nsin;
    private final String checkNum;

    public ISIN(String isin) {
        assert isin.length() == 12;
        this.countryCode = isin.substring(0, 2);
        this.nsin = isin.substring(2, 10);
        this.checkNum = isin.substring(10, 12);
    }

    @Override
    public String format() {
        return String.format("%s%s%s", countryCode, nsin, checkNum);
    }
}
