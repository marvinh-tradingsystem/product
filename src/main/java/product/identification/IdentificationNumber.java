package product.identification;

public interface IdentificationNumber {
    String format();
}
